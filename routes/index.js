var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var filter = require('../filters');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'cancel',
    password: 'fruitfresh',
    database: 'workhard'
});
connection.connect();
/* GET home page. */
router.get('/', function (req, res, next) {
    //console.log('Cookie=' + req.cookies.Cancel);
    if (req.cookies.cooc == '1') {
        res.render('index', {title: 'Express', cName: 'Cancel'});
    } else {
        res.redirect('/login');
    }
});

router.all('/login', function (req, res, next) {
    var us = req.body.user;
    var ps = req.body.passwd;
    connection.query(
        'SELECT * from users WHERE (users.username = ?)AND(workhard.users.userpass = ?)', [us, ps], function (err, rows, fields) {
            if (err || !rows.length) {
                res.render('login');
            } else {
                //console.log('TEST: ', rows[0], rows.length, req.body.user);
                if (rows[0].userpass == ps) {
                    res.cookie('cooc', 1);
                    res.redirect('/');
                }
            }
        });
});

router.all('/bookAdd', function (req, res, next) {
    res.render('bookAdd');
});

router.get('/logoff', function (req, res, next) {
    res.clearCookie('cooc');
    res.redirect('/');
});

router.all('/library', function (req, res, next){
    //console.log(req.query.str ? req.query.str : '');
    var searchStr = '%' + (req.query.str ? req.query.str : '') + '%';
    //console.log(searchStr);
    connection.query(
        'SELECT * from workhard.books WHERE (author LIKE ?)OR(bookname LIKE ?)OR(year LIKE ?)OR(link LIKE ?) ',[searchStr,searchStr,searchStr,searchStr], function (err, rows, fields) {
            if (err || !rows.length) {
                res.render('library');
            } else {
                filter.Test(res, rows);
                //res.render('library', {data: rows});
            }
        });

});
module.exports = router;
